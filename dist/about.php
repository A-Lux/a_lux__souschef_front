<?php include('header.php');?> 
<?php include('modal-basket.php');?> 
<?php include('whatsapp-modal.php');?> 
<div class="about">
    <div class="container">
        <div class="row about-row">
            <div class="col-xl-9">
        <div class="about-content title">
            <h1>Как это работает?</h1>
            <span>В воскресенье с 10:00 до 22:00 в любой удобный Вам двух часовой интервал, мы доставим коробку с продуктами и рецептами для приготовления ужинов.</span>
            <h2>Открываем коробку:</h2>
            <img src="image/about1.png" alt="">
            <p>В день доставки Вы получите фирменную коробку весом около семи килограммов. </p>
            <p>Внутри много вкусного</p>
        </div>
        <div class="about-content-inner">
            <div class="row">
                <div class="col-xl-5">
                    <div class="content-image">
                    <img src="image/about-2.png" alt="">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="content-text">
                        <p>На каждом пакете есть название блюда и дата, до которой его нужно приготовить.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-content">
            <img src="image/about-3.png" alt="">
            <p>Продуктов всегда столько, сколько нужно. Ничего лишнего. </p>
        </div>
        <div class="about-content-inner">
            <div class="row">
                <div class="col-xl-5">
                    <div class="content-image">
                    <img src="image/about-4.jpg" alt="">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="content-text">
                        <p>Мясо и молочная продукция лежат в специальном термопакете чтобы поддерживать нужную температуру во время доставки.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-content">
            <img src="image/about-5.png" alt="">
            <p>Мы не доставляем замороженные мясо и рыбу, только охлажденные. Чтобы они сохранили свежесть до Вашего ужина, мы фасуем их в вакуумную упаковку со специальной салфеткой, впитывающей мясной сок.</p>
        </div>
        <div class="about-content-inner">
            <div class="row">
                <div class="col-xl-5">
                    <div class="content-image">
                    <img src="image/about-6.png" alt="">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="content-text">
                        <p>На рецепте есть вся нужная информация: ингредиенты, вес одной порции, время приготовления и список посуды, которая вам понадобится.</p>
                        <p>Процесс приготовления описан по этапам и с фотографиями.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-content">
            <img src="image/about-7.png" alt="">
            <p>Мы доставляем продукты и рецепты до Вашего дома в специально оборудованном авто со встроенным холодильником. В результате во время доставки все продукты едут в комфортной для них температуре.</p>
            <p class="p-about">Готовить – одно удовольствие! </p>
            <p class="p-about">С любовью, Ваш СуШЕФ </p>
        </div>
    </div>
    <div class="col-xl-12">
        <div class="about-video">
        <iframe width="1180" height="664" src="https://www.youtube.com/embed/F2fCJqzKNng" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
    </div>
    </div>
</div>

<?php include('footer.php');?>  