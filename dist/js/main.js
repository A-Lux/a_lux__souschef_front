$(document).ready(function(){
    $('.burger-menu-btn').click(function(e){
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
});
$(function(){
  $("#phone1").mask("8(999) 999-9999");
});
$('.receive-code').on('click', function(){
  $('.enter-code').toggleClass('enter-code-active');
});
// window.onscroll=(function(){
//     if(document.documentElement.scrollTop>=120){
//         document.getElementById('choose-head').className='choose-head-active';      
//     }
//     else{
//        document.getElementById('choose-head').className='choose-head';
//     }
// })

// BURGER-MENU
$('.js-toggle-menu').click(function(e){
  e.preventDefault();
  $('.hamburger-menu').toggleClass('hamburger-menu-active');
  $('.mobile-header-nav').slideToggle();
  $(this).toggleClass('open');
});
$('.ast-menu-toggle').click(function(e){
  e.preventDefault();
  $('.ast-menu-btn').toggleClass('ast-menu-btn-active');
  $('.sub-menu').toggleClass('sub-menu-active');
})
var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
      maxWidth: false,
      customToggle: $toggle,
      navTitle: 'Ваша корзина',
      levelTitles: true,
      insertClose: 2,
      closeLevels: false,
      position: 'right',
      insertClose: false,
      labelBack: 'Назад'

    };
    $main_nav.hcOffcanvasNav(defaultData);
$('.menu').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
});
$('.orders-slider').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.orders-slider-bottom').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.slider-nav').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.slide-content',
});
$('.slide-content').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  swipe: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.review-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots:true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  
  $('.original-slider').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 1,
  // swipe: false,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        // slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: false,
        slidesToShow: 1,
        centerPadding: '0px',
      }
    }
  ]
});
$('.choose-prise').hover(
  function(){
  $('.choose-button').show();
  },
  function(){
  $('choose-button').hide();
  }
  );
  
let size = 218;
let text = $('.review-p');
    text.each(function () {
        if ($(this).text().length > size) {
            $(this).text($(this).text().slice(0, size) + '...')
        }
    });
    document.getElementById('time-sending').innerText = new Date().getHours() + ':' + new Date().getMinutes();
});
$('#opener').on('click', function() {		
  var panel = $('#slide-panel');

  if (panel.hasClass("visible")) {
    panel.removeClass('visible').animate({'margin-right':'-300px'});
    // $('body').css('opacity', '1')
  } else {
    panel.addClass('visible').animate({'margin-right':'0px'});
    // $('body').css('background', 'black');
    // $('body').css('opacity', '0.7')
  }	
  return false;	
});

$(document).ready(function() {
  $('.select-adress').select2({
    placeholder: "ВЫБЕРИТЕ АДРЕС",
    minimumResultsForSearch: Infinity
  });
  $('.dishes-quantity').select2({
    minimumResultsForSearch: Infinity
  });
 
  $('.quantity-person').select2({
    minimumResultsForSearch: Infinity
  });
  
  $("#openmodal").on("change", function () {        
    if($(this).val() === 'openmodaloption'){
       $('#modal').modal('show');
   }
});
$('.delivery-select').select2({
  placeholder: "НЕТ ДОСТАВКИ НА УКАЗАННЫЙ АДРЕС",
  minimumResultsForSearch: Infinity
});
$('.time-select').select2({
  placeholder: "НЕТ ИНТЕРВАЛОВ ДОСТАВКИ",
  minimumResultsForSearch: Infinity
});
$('.show-dishes').on('click', function(){
  var spolier = $(this).parents('.text-overflow').find('.read-overflow').toggle('slow');
  $('.spoiler-arrow').toggleClass('spoiler-arrow-active');
if ( this.value == 'Посмотреть блюда' ) { 
  this.value = 'Скрыть';
} 
  else { 
  this.value = "Посмотреть блюда"; 
  };
return false;
});
$('.close-panel').on('click', function(){
  $('.scissors').css('display', 'none');
});
});
$('.help-icon').on('click', function(){
  $('.tooltip-card').toggleClass('tooltip-card-active');
});
$('.expand-btn').on('click', function(){
  $('.expand-wrapper').toggleClass('expand-wrapper-active');
});
$('.question-arrow').on('click', function(){
  $('.collapse').toggleClass('show');
  $('.question-arrow ').toggleClass('question-arrow-active');
});

// $('.js-example-basic-single').on('select2:select', function(){
  
// });

// $('.address').select2()
// $('.delivery').select2({
// })
// $('.time').select2({
// })
// $('.address').on('select2:select', function() {
//   Swal.fire( {
//     html: `
//       <div id="map"></div>
//       <script>
//         y
//       </script>
//       <input>
//     `
//   }).then(
//     axios.get(url)
//       .then(function(data.id) {
//         let id = data.id;
//         let date = new Date(data.date);
//         var newOption = new Option(date, id, false, false);
//         $('.delivery').prop('disabled', 'false').append(newOption).trigger('change');
//       })
//     )
// })
// {
//   [
//     id: 1,
//     date: 123123123123123123,
//   ],
//   [

//   ],
// }