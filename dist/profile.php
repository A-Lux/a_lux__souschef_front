<?php include('header.php');?> 
<div class="profile-page">
    <div class="container profile-container">
        <div class="row">
            <div class="col-12 col-xl-4">
                <div class="profile-info-left">
                    <div class="profile-list">
                        <div class="profile-info">
                                <div class="row">
                                    <div class="col-xl-9">
                                        <div class="profile-title">
                                        <h1>Лаура Увальжанова</h1>
                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                    <div class="profile-edit">
                                        <a href=""><img src="../dist/image/profile-edit.png" alt=""></a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <a href="">+7 925 871 95 20</a>
                            <p>Мичуринский проспект, 25 кв 90Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                        <div class="list-group profile-list-group" id="list-tab" role="tablist">
                            <div class="tabs-title">
                                <h1>Ваши заказы</h1>
                            </div>
                        <a class="list-group-item profile-group-item list-group-item-action active" id="list-orders-list" data-toggle="list" href="#list-orders" role="tab" aria-controls="orders">Ближайшие заказы <i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        <div class="tabs-title">
                            <h1>Настройки подписки</h1>
                        </div>
                        <a class="list-group-item profile-group-item list-group-item-action" id="list-card-list" data-toggle="list" href="#list-card" role="tab" aria-controls="card">Мои карты<i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        <a class="list-group-item profile-group-item list-group-item-action" id="list-adress-list" data-toggle="list" href="#list-adress" role="tab" aria-controls="adress">Мои адреса <i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        <div class="tabs-title">
                            <h1>Полезная информация</h1>
                        </div>
                    <a class="list-group-item profile-group-item list-group-item-action" id="list-terms-list" data-toggle="list" href="#list-terms" role="tab" aria-controls="terms">Условия доставки и оплаты<i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    <a class="list-group-item profile-group-item list-group-item-action" id="list-agreement-list" data-toggle="list" href="#list-agreement" role="tab" aria-controls="agreement">Пользовательское соглашение <i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-8">
                <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-orders" role="tabpanel" aria-labelledby="list-orders-list">
                    <div class="orders-slider">
                        <div class="order-slide">
                            <div class="orderTimeDiv">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="order-date">
                                            <p>9 февраля</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="order-time">
                                            <span>С 18.00 до 21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="orders-item">
                                <div class="order-image">
                                    <img src="../dist/image/order1.png" alt="">
                                </div>
                                <div class="order-info">
                                    <h1>Сбалансированное меню на 2-х, 5 ужинов, 12 блюд</h1>
                                </div>
                                <div class="order-cost">
                                    <span>4790 ₸</span>
                                </div>
                                <div class="order-process">
                                    <div class="row">
                                        <div class="col-xl-6">
                                           <div class="process-info">
                                               <p>В работе</p>
                                           </div> 
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="cansel-button">
                                                <a href="">ОТМЕНИТЬ</a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-slide">
                            <div class="orderTimeDiv">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="order-date">
                                            <p>9 февраля</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="order-time">
                                            <span>С 18.00 до 21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="orders-item">
                                <div class="order-image">
                                    <img src="../dist/image/order1.png" alt="">
                                </div>
                                <div class="order-info">
                                    <h1> Сбалансированное меню на 2-х, 5 ужинов, 12 блюд</h1>
                                </div>
                                <div class="order-cost">
                                    <span>4790 ₸</span>
                                </div>
                                <div class="order-process">
                                    <div class="row">
                                        <div class="col-xl-6">
                                           <div class="process-info">
                                               <p>В работе</p>
                                           </div> 
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="cansel-button">
                                                <a href="">ОТМЕНИТЬ</a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-slide">
                            <div class="orderTimeDiv">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="order-date">
                                            <p>9 февраля</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="order-time">
                                            <span>С 18.00 до 21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="orders-item">
                                <div class="order-image">
                                    <img src="../dist/image/order1.png" alt="">
                                </div>
                                <div class="order-info">
                                    <h1>Сбалансированное меню на 2-х, 5 ужинов, 12 блюд</h1>
                                </div>
                                <div class="order-cost">
                                    <span>4790 ₸</span>
                                </div>
                                <div class="order-process">
                                    <div class="row">
                                        <div class="col-xl-6">
                                           <div class="process-info">
                                               <p>В работе</p>
                                           </div> 
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="cansel-button">
                                                <a href="">ОТМЕНИТЬ</a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="orders-slider-bottom">
                        <div class="order-slide">
                            <div class="orderTimeDiv">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="order-date">
                                            <p>9 февраля</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="order-time">
                                            <span>С 18.00 до 21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="orders-item">
                                <div class="order-image">
                                    <img src="../dist/image/order2.png" alt="">
                                </div>
                                <div class="order-info">
                                    <h1>Сбалансированное меню на 2-х, 5 ужинов, 12 блюд</h1>
                                </div>
                                <div class="order-cost">
                                    <span>4790 ₸</span>
                                </div>
                                <div class="order-process">
                                    <div class="row">
                                        <div class="col-xl-6">
                                           <div class="process-info">
                                               <p>Отменен</p>
                                           </div> 
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="reestablish-button">
                                                <a href="">Восстановить</a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-slide">
                            <div class="orderTimeDiv">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="order-date">
                                            <p>9 февраля</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="order-time">
                                            <span>С 18.00 до 21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="orders-item">
                                <div class="order-image">
                                    <img src="../dist/image/order2.png" alt="">
                                </div>
                                <div class="order-info">
                                    <h1>Сбалансированное меню на 2-х, 5 ужинов, 12 блюд</h1>
                                </div>
                                <div class="order-cost">
                                    <span>4790 ₸</span>
                                </div>
                                <div class="order-process">
                                    <div class="row">
                                        <div class="col-xl-6">
                                           <div class="process-info">
                                               <p>Отменен</p>
                                           </div> 
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="reestablish-button">
                                                <a href="">Восстановить</a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-slide">
                            <div class="orderTimeDiv">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="order-date">
                                            <p>9 февраля</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="order-time">
                                            <span>С 18.00 до 21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="orders-item">
                                <div class="order-image">
                                    <img src="../dist/image/order2.png" alt="">
                                </div>
                                <div class="order-info">
                                    <h1>Сбалансированное меню на 2-х, 5 ужинов, 12 блюд</h1>
                                </div>
                                <div class="order-cost">
                                    <span>4790 ₸</span>
                                </div>
                                <div class="order-process">
                                    <div class="row">
                                        <div class="col-xl-6">
                                           <div class="process-info">
                                               <p>Отменен</p>
                                           </div> 
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="reestablish-button">
                                                <a href="">Восстановить</a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-history" role="tabpanel" aria-labelledby="list-history-list">
                  
                </div>
                <div class="tab-pane fade" id="list-card" role="tabpanel" aria-labelledby="list-card-list">
                   <div class="bank-cards">
                       <h1>Банковские карты</h1>
                       <a href="" type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter">+ Добавить карту</a>
                        <div class='modal fade adress-modal' id="exampleModalCenter" tabindex="-1" 
                        role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="new-adress">
                                <h4 class="modal-title">Добавление новой карты</h4>
                                <p>В целях безопасности мы не храним данные вашей банковской карты</p>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                            <div class="card-info-body">
                            <div class="payment-method">
                            <div class="card-left">
                            <input type="text" class="payment-input" placeholder="НОМЕР КАРТЫ">
                            <input type="text" class="payment-input" placeholder="ИМЯ ВЛАДЕЛЬЦА">
                            <div class="payment-info">
                                <div class="row">
                                <div class="col-xl-6">
                                    <div class="control">
                                    <input type="text" class="payment-input" placeholder="ММ/ГГ">
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="control-image">
                                    <img src="../dist/image/pay.png" alt="">
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="card-right">
                            <div class="card-stripe"></div>
                            <div class="card-info">
                                <input class="payment-input" type="text" placeholder="EMAIL(ДЛЯ ОТПРАВКИ ЧЕКА)">
                            <div class="card-info-bottom">
                                <input class="payment-input" type="text" placeholder="CVC/CVV">
                                <span class="help-icon"> ? </span>
                                <div class="tooltip-card"> 
                                <p> <b>Код</b> проверки подлинности карты (или <b>код CVC*</b>) - это дополнительный код, нанесенный на 
                                    вашу дебетовую или кредитную карту. Для большинства карт (Visa, MasterCard, банковские карты и т. д.) это
                                    последние три цифры числа, напечатанного в поле подписи <b>на обратной стороне карты.</b></p>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        <div class="cards-icon">
                            <img src="../dist/image/cards.svg" alt="">
                        </div>
                        <div class="payment-text">
                            <p>Для проверки подлинности карты мы спишем и вернем 1 рубль. Ваша карта будет автоматически привязана к профилю, для удобства платежей. 
                            Вы в любой момент можете отвязать или поменять её на другую в личном кабинете.</p>
                        </div>
                        <div class="add-card-btn">
                            <button>Добавить карту</button>
                        </div>
                        </div>
                        </div>
                            </div>
                            </div>
                        </div>
                        <div class="security">
                            <p>Это безопасно - наш сайт защищен и постоянно мониторится</p>
                            <img src="../dist/image/cards.svg" alt="">
                        </div>
                    </div> 
                </div>
                <div class="tab-pane fade" id="list-adress" role="tabpanel" aria-labelledby="list-adress-list">
                <div class="adress-inner">
                        <h1>Адреса доставки</h1>
                        <div class="adress-item">
                            <div class="row">
                                <div class="col-xl-8">
                                    <div class="adress-text">
                                    <p>Москва, Смирновская улица,</p>
                                    <a href="">+7 (905) 724-75-05</a>
                                    <a href="" type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter-1">+ Добавить новый адрес</a>
                                    <div class='modal fade adress-modal' id="exampleModalCenter-1" tabindex="-1" 
                                    role="dialog" aria-labelledby="exampleModalCenterTitle-1" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                        <div class="new-adress">
                                            <h4 class="modal-title">Добавить новый адрес</h4>
                                            <p>Укажите точку на карте или заполните поля</p>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="map">
                                            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor" 
                                            width="763" height="300" frameborder="0"></iframe>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="new-adress-info">
                                            <div class="row">
                                                <div class="col-xl-8">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Город, улица, дом">
                                                </div>
                                                </div>
                                                <div class="col-xl-4">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Квартира">
                                                </div>
                                                </div>
                                                <div class="col-xl-4">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Подъезд">
                                                </div>
                                                </div>
                                                <div class="col-xl-4">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Этаж">
                                                </div>
                                                </div>
                                                <div class="col-xl-4">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Домофон">
                                                </div>
                                                </div>
                                                <div class="col-xl-6">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Имя получателя">
                                                </div>
                                                </div>
                                                <div class="col-xl-6">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="+7 (905) 724-75-05">
                                                </div>
                                                </div>
                                                <div class="col-xl-12">
                                                <div class="new-adress-input">
                                                    <input type="text" placeholder="Комментарий">
                                                </div>
                                                </div>
                                                <div class="col-xl-12">
                                                <div class="add-adress">
                                                    <a href="">Добавить</a>
                                                </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                      </div>
                                    </div>
                                    </div>
                                </div>
                            <div class="col-xl-4 col-12">
                                <div class="adress-edit">
                                    <a href="" type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter-2">Изменить</a>
                                    <div class='modal fade adress-modal' id="exampleModalCenter-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="new-adress">
                        <h4 class="modal-title">Изменение адреса и получателя</h4>
                        <p>Укажите точку на карте или заполните поля</p>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="map">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor" 
                        width="763" height="300" frameborder="0"></iframe>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <div class="new-adress-info">
                          <div class="row">
                            <div class="col-xl-8">
                              <div class="new-adress-input">
                                <input type="text" placeholder="Город, улица, дом">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Квартира">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Подъезд">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Этаж">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Домофон">
                              </div>
                            </div>
                            <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Имя получателя">
                              </div>
                            </div>
                            <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" placeholder="+7 (905) 724-75-05">
                              </div>
                            </div>
                            <div class="col-xl-12">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Комментарий">
                              </div>
                            </div>
                            <div class="col-xl-12">
                              <div class="add-adress">
                                <a href="">Сохранить изменения</a>
                                <div class="delete-adress">
                                    <a href="">Удалить адрес</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-terms" role="tabpanel" aria-labelledby="list-terms-list">
                    <div class="terms">
                        <div class="terms-title">
                        <h1>Пользовательское соглашение (договор о присоединении)</h1>
                        <p>Редакция № 18 от 16 мая 2019 г. г. Москва</p>
                        </div>
                        <div class="terms-text">
                            <h2>1. Общие положения и предмет Соглашения.</h2>
                            <div class="sub-terms">
                                <p>2.1. Продавец предлагает Покупателю возможность заказа, оплаты и доставки Товаров через Интернет-магазин, по телефону или через иные средства связи.</p>
                                <p>2.2. Предметом настоящего Соглашения являются условия заключения с Покупателем договора дистанционной купли-продажи Товаров и 
                                    их доставки в соответствии с условиями Соглашения и за цену, установленную Продавцом на Товары в Интернет-магазине, 
                                    а также порядок оплаты и приемки Товаров Покупателем в соответствии с условиями настоящего Соглашения и 
                                    Условиями доставки.</p>
                                <p>2.3. Покупатель, оформляя заказ на Товары через Интернет-магазин либо посредством телефонной связи, принимает все 
                                    условия настоящего Соглашения (без полных или частичных изъятий). В случае несогласия с условиями Соглашения 
                                    полностью либо в его части, Покупатель обязан отказаться от покупки Товаров.</p>
                                <p> 2.4. Условия Соглашения могут быть изменены Продавцом без какого-либо специального 
                                    уведомления Покупателя. Действие новой редакции Соглашения начинается по истечении 3 
                                    (трех) дней с момента ее размещения на соответствующей интернет-странице.</p>
                                <p>2.5. Настоящее Соглашение является публичной офертой. Размещение заказа на Товар 
                                    Покупателем любым способом или намерения заказать Товар, участие Покупателя в рекламных 
                                    акциях Продавца, а также регистрация Покупателя в Интернет-магазине, являются акцептом 
                                    оферты, что означает заключение договора дистанционной купли-продажи на условиях, 
                                    изложенных в настоящем Соглашении. Договор считается заключенным с момента подтверждения 
                                    оформленного Покупателем заказа Товаров, принятия Покупателем участия в рекламной акции 
                                    Продавца путем направления Продавцу контактных данных в любом виде, или регистрации в 
                                    Интернет-магазине. Продавец обязуется выдать Покупателю кассовый и/или товарный чек, либо 
                                    иной документ подтверждающий факт оплаты, а право собственности на заказанный Покупателем 
                                    Товар переходит к нему в момент получения Товара, но не ранее момента полной оплаты Товара
                                     Покупателем.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-agreement" role="tabpanel" aria-labelledby="list-agreement-list">
                    <div class="terms">
                        <div class="terms-title">
                        <h1>Пользовательское соглашение (договор о присоединении)</h1>
                        <p>Редакция № 18 от 16 мая 2019 г. г. Москва</p>
                        </div>
                        <div class="terms-text">
                            <h2>1. Общие положения и предмет Соглашения.</h2>
                            <div class="sub-terms">
                                <p>2.1. Продавец предлагает Покупателю возможность заказа, оплаты и доставки Товаров через Интернет-магазин, по телефону или через иные средства связи.</p>
                                <p>2.2. Предметом настоящего Соглашения являются условия заключения с Покупателем договора дистанционной купли-продажи Товаров и 
                                    их доставки в соответствии с условиями Соглашения и за цену, установленную Продавцом на Товары в Интернет-магазине, 
                                    а также порядок оплаты и приемки Товаров Покупателем в соответствии с условиями настоящего Соглашения и 
                                    Условиями доставки.</p>
                                <p>2.3. Покупатель, оформляя заказ на Товары через Интернет-магазин либо посредством телефонной связи, принимает все 
                                    условия настоящего Соглашения (без полных или частичных изъятий). В случае несогласия с условиями Соглашения 
                                    полностью либо в его части, Покупатель обязан отказаться от покупки Товаров.</p>
                                <p> 2.4. Условия Соглашения могут быть изменены Продавцом без какого-либо специального 
                                    уведомления Покупателя. Действие новой редакции Соглашения начинается по истечении 3 
                                    (трех) дней с момента ее размещения на соответствующей интернет-странице.</p>
                                <p>2.5. Настоящее Соглашение является публичной офертой. Размещение заказа на Товар 
                                    Покупателем любым способом или намерения заказать Товар, участие Покупателя в рекламных 
                                    акциях Продавца, а также регистрация Покупателя в Интернет-магазине, являются акцептом 
                                    оферты, что означает заключение договора дистанционной купли-продажи на условиях, 
                                    изложенных в настоящем Соглашении. Договор считается заключенным с момента подтверждения 
                                    оформленного Покупателем заказа Товаров, принятия Покупателем участия в рекламной акции 
                                    Продавца путем направления Продавцу контактных данных в любом виде, или регистрации в 
                                    Интернет-магазине. Продавец обязуется выдать Покупателю кассовый и/или товарный чек, либо 
                                    иной документ подтверждающий факт оплаты, а право собственности на заказанный Покупателем 
                                    Товар переходит к нему в момент получения Товара, но не ранее момента полной оплаты Товара
                                     Покупателем.</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?> 