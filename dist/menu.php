<?php include('header.php');?>
<div class="menu-page">
    <div class="container">
        <div class="slider-nav">
            <div class="menu-slide">
                <h1>Меню на 27 февраля - 4 марта</h1>
                <p>Доставка этого меню доступна только с 1 по 4 марта </p>
            </div>
            <div class="menu-slide">
                <h1>Меню на 27 февраля - 4 марта</h1>
                <p>Доставка этого меню доступна только с 1 по 4 марта </p>
            </div>
        </div>
    </div>
    <div class="menu-tabs">
        <div class="container">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">
               Оригинальное 
               <p>ХИТ</p>
            </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-family-tab" data-toggle="pill" href="#pills-family" role="tab" aria-controls="pills-family" aria-selected="false">Семейное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link minutes" id="pills-fast-tab" data-toggle="pill" href="#pills-fast" role="tab" aria-controls="pills-fast" aria-selected="false">20 минут</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-balans-tab" data-toggle="pill" href="#pills-balans" role="tab" aria-controls="pills-balans" aria-selected="false">Баланс</a>
                </li>
            </ul>
        </div>
    </div>
</div>
    <div class="slide-content">
        <div class="slider-content">
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10  col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Запечённая говядина в горшочке с картофелем и черносливом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                                <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10">
                                            <div class="dish-title">
                                                <h1>Свиная корейка с яблоком на гриле в бальзамической глазури с воздушным картофельным пюре</h1>
                                                <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Паста пенне с итальянским соусом Аррабиата и сыром пармезан</h1>
                                                <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10">
                                            <div class="dish-title">
                                                <h1>Индейка по-сычуаньски со спагетти из шпината</h1>
                                                <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Северная Америка</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Сочный гавайский бургер со свининой, жареным ананасом и летним салатом</h1>
                                                <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-family" role="tabpanel" aria-labelledby="pills-family-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Люля-кебаб из индейки с запечёнными картофельными дольками с тимьяном и домашним соусом тартар</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image//fam2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Азия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Запечённые куриные бёдрышки в сладкой паприке с соевым соусом и кинзой с гречневой лапшой</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Ризотто со сливками, сыром пармезан и запеченной тыквой в пряных травах </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Крокеты из индейки со сметанным соусом и гречкой с овощами</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Медальоны из свинины в соусе сливочный демиглас с обжаренным картофелем и вялеными томатами </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-fast" role="tabpanel" aria-labelledby="pills-fast-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Запечённая говядина в горшочке с картофелем и черносливом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12">
                                            <div class="dish-title">
                                                <h1>Свиная корейка с яблоком на гриле в бальзамической глазури с воздушным картофельным пюре</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Паста пенне с итальянским соусом Аррабиата и сыром пармезан</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Индейка по-сычуаньски со спагетти из шпината</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Северная Америка</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Сочный гавайский бургер со свининой, жареным ананасом и летним салатом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-balans" role="tabpanel" aria-labelledby="pills-balans-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Люля-кебаб из индейки с запечёнными картофельными дольками с тимьяном и домашним соусом тартар</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image//fam2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Азия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Запечённые куриные бёдрышки в сладкой паприке с соевым соусом и кинзой с гречневой лапшой</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Ризотто со сливками, сыром пармезан и запеченной тыквой в пряных травах </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Крокеты из индейки со сметанным соусом и гречкой с овощами</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Медальоны из свинины в соусе сливочный демиглас с обжаренным картофелем и вялеными томатами </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
       <div class="slider-content">
       <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Запечённая говядина в горшочке с картофелем и черносливом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Свиная корейка с яблоком на гриле в бальзамической глазури с воздушным картофельным пюре</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Паста пенне с итальянским соусом Аррабиата и сыром пармезан</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Индейка по-сычуаньски со спагетти из шпината</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Северная Америка</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Сочный гавайский бургер со свининой, жареным ананасом и летним салатом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-family" role="tabpanel" aria-labelledby="pills-family-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Люля-кебаб из индейки с запечёнными картофельными дольками с тимьяном и домашним соусом тартар</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image//fam2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Азия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Запечённые куриные бёдрышки в сладкой паприке с соевым соусом и кинзой с гречневой лапшой</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Ризотто со сливками, сыром пармезан и запеченной тыквой в пряных травах </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Крокеты из индейки со сметанным соусом и гречкой с овощами</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Медальоны из свинины в соусе сливочный демиглас с обжаренным картофелем и вялеными томатами </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-fast" role="tabpanel" aria-labelledby="pills-fast-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-10">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Запечённая говядина в горшочке с картофелем и черносливом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Свиная корейка с яблоком на гриле в бальзамической глазури с воздушным картофельным пюре</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Паста пенне с итальянским соусом Аррабиата и сыром пармезан</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Индейка по-сычуаньски со спагетти из шпината</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Северная Америка</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 4 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Сочный гавайский бургер со свининой, жареным ананасом и летним салатом</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-balans" role="tabpanel" aria-labelledby="pills-balans-tab">
                <div class="original">
                    <div class="container">
                        <div class="original-slider">
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam1.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Россия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10">
                                            <div class="dish-title">
                                                <h1>Люля-кебаб из индейки с запечёнными картофельными дольками с тимьяном и домашним соусом тартар</h1>
                                                <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image//fam2.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Азия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Запечённые куриные бёдрышки в сладкой паприке с соевым соусом и кинзой с гречневой лапшой</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam3.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Без духовки</span>
                                        <span>#Италия</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 7 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Ризотто со сливками, сыром пармезан и запеченной тыквой в пряных травах </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam4.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Китай</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Крокеты из индейки со сметанным соусом и гречкой с овощами</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/fam5.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 5 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-md-10 col-12">
                                            <div class="dish-title">
                                                <h1>Медальоны из свинины в соусе сливочный демиглас с обжаренным картофелем и вялеными томатами </h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="original-slide">
                                <div class="menu-slide-image">
                                    <img src="image/menu6.jpeg" alt="">
                                    <div class="menu-hashtags">
                                        <span>#До 20 минут</span>
                                        <span>#Без духовки</span>
                                        <span>#Европа</span>
                                    </div>
                                </div>
                                <div class="menu-content">
                                    <div class="row">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="time-cook">
                                                <p>Готовим на</p>
                                                <h1>1 - 3 день</h1>
                                            </div>
                                        </div>
                                        <div class="col-xl-10 col-12 col-md-10">
                                            <div class="dish-title">
                                                <h1>Спагетти неро с лососем, жёлтым болгарским перцем и базиликом в сливочном терияки</h1>
                                                <div class="time-cook-mob">
                                                <p>Готовим на <b>1 - 7 день</b></p>
                                            </div>
                                            <div class="dish-info">
                                                    <div class="dish-icons">
                                                    <div class="dish-icon">
                                                        <p><img src="image/cook.JPG" alt=""> 450 г</p>
                                                    </div>
                                                    <div class="dish-icon">
                                                        <p><img src="image/time.JPG" alt=""> 40 мин</p>
                                                    </div>
                                                    <div class="dish-icon dish-time">
                                                        <p><img src="image/shef.JPG" alt=""> Готовить непросто</p>
                                                    </div>
                                                    </div>
                                                    <div class="dish-value dish-icon">
                                                        <p>На 100 г блюда: 139 кКал, 8.0 | 7.0 | 11.0 БЖУ
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-buttons">
                        <a href="menu-inner.php" class="watch">Смотреть меню</a>
                        <a href="choose.php" class="compare">Сравнить меню</a>
                    </div>
                </div>
            </div>
        </div>
       </div>
        
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <?php include('footer.php');?>