<?php include('header.php');?> 
<?php include('modal-basket.php');?> 
<?php include('whatsapp-modal.php');?> 
<div class="about-recipes">
    <div class="container">
        <div class="row about-recipes-row">
            <div class="col-xl-6 col-12 col-md-6">
                <div class="about-recipes-text">
                    <h1>3 вида наборов</h1>
                    <p>Получайте коробку с продуктами и рецептами для ужинов каждую неделю и готовьте дома. Пусть каждый ужин будет маленьким праздником!</p>
                    <a href=""><i class="fas fa-shopping-cart"></i>НАЧНИТЕ ГОТОВИТЬ СЕЙЧАС</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="advantage">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="fas fa-vote-yea"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>Выбирайте меню</h2>
                        <p>Каждую неделю 30+ блюд от шеф-поваров и диетологов</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="fas fa-truck-moving"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>Свежие продукты</h2>
                        <p>Расфасованные ингредиенты с пошаговыми фоторецептами</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="far fa-clock"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>Блюда за 15-40 минут</h2>
                        <p>Шедевры с любовью! Готовить - одно удовольствие!</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="fas fa-recycle"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>Никаких отходов</h2>
                        <p>Скажите "нет" испорченным продуктам и загрязнению природы</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-recipes-2">
    <div class="container">
        <div class="row about-recipes-2-row">
            <div class="col-xl-4 p-0 col-6">
                <div class="about-recipes-2-text">
                    <h1>30+ РЕЦЕПТОВ ЕЖЕНЕДЕЛЬНО</h1>
                    <p>Выбирайте вкуснейшие рецепты от шеф-поваров и диетологов</p>
                    <a href=""><i class="far fa-gem"></i>СМОТРЕТЬ МЕНЮ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-recipes-3">
    <div class="container">
        <div class="row about-recipes-3-row">
            <div class="col-xl-4 p-0 col-7 col-md-6">
                <div class="about-recipes-3-text">
                    <h1>КАЧЕСТВЕННЫЕ ИНГРИДИЕНТЫ</h1>
                    <p>Готовьте с лучшими ингредиентами от наших партнеров и поставщиков.</p>
                    <a href=""><i class="fas fa-box-open"></i>ПОСТАВЩИКИ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-recipes-4">
    <div class="container">
        <div class="row about-recipes-4-row">
            <div class="col-xl-5 col-7 col-md-6">
                <div class="about-recipes-4-text">
                    <h1>МЕНЮ РАЗРАБОТАНО ШЕФ-ПОВАРАМИ И УТВЕРЖДЕНО ДИЕТОЛОГАМИ</h1>
                    <p>Наша команда талантливых поваров и диетологов создает еженедельное меню, полное рецептов здорового ужина.</p>
                    <a href=""><i class="fas fa-users"></i>НАША КОМАНДА</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="review">
    <div class="container">
        <div class="review-logo-image">
            <img src="image/index-logo.png" alt="">
        </div>
        <div class="review-top">
            <h1>Отзывы Клиентов</h1>
            <img src="image/contact-2.png" alt="">
        </div>
        <div class="review-slider-title">
            <h2>What our clients say</h2>
        </div>
        <div class="review-slider">
            <div class="review-slide">
                <div class="review-title">
                    <img src="image/1.webp" alt="">
                    <div class="review-name">
                        <h1>Taylor Jay <i class="fab fa-facebook-f"></i></h1>
                        <p>Founder & CEO</p>
                    </div>
                </div>
                <div class="review-logo">
                    <img src="image/review1.webp" alt="">
                </div>
                <div class="rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
                <div class="review-comment">
                    <p class="review-p">Love the design and customization of InstaShow ?. We have used various Instagram apps for Shopify in the past but they would always mess up our theme. I love how we can completely customize InstaShow to how we want the feed to come out. Loads quickly and responsive to all devices. A++</p>
                </div>
            </div>
            <div class="review-slide">
                <div class="review-title">
                    <img src="image/1.webp" alt="">
                    <div class="review-name">
                        <h1>Taylor Jay <i class="fab fa-facebook-f"></i></h1>
                        <p>Founder & CEO</p>
                    </div>
                </div>
                <div class="review-logo">
                    <img src="image/review1.webp" alt="">
                </div>
                <div class="rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
                <div class="review-comment">
                    <p class="review-p">Love the design and customization of InstaShow ?. We have used various Instagram apps for Shopify in the past but they would always mess up our theme. I love how we can completely customize InstaShow to how we want the feed to come out. Loads quickly and responsive to all devices. A++</p>
                </div>
            </div>
            <div class="review-slide">
                <div class="review-title">
                    <img src="image/1.webp" alt="">
                    <div class="review-name">
                        <h1>Taylor Jay <i class="fab fa-facebook-f"></i></h1>
                        <p>Founder & CEO</p>
                    </div>
                </div>
                <div class="review-logo">
                    <img src="image/review1.webp" alt="">
                </div>
                <div class="rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
                <div class="review-comment">
                    <p class="review-p">Love the design and customization of InstaShow ?. We have used various Instagram apps for Shopify in the past but they would always mess up our theme. I love how we can completely customize InstaShow to how we want the feed to come out. Loads quickly and responsive to all devices. A++</p>
                </div>
            </div>
            <div class="review-slide">
                <div class="review-title">
                    <img src="image/1.webp" alt="">
                    <div class="review-name">
                        <h1>Taylor Jay <i class="fab fa-facebook-f"></i></h1>
                        <p>Founder & CEO</p>
                    </div>
                </div>
                <div class="review-logo">
                    <img src="image/review1.webp" alt="">
                </div>
                <div class="rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
                <div class="review-comment">
                    <p class="review-p">Love the design and customization of InstaShow ?. We have used various Instagram apps for Shopify in the past but they would always mess up our theme. I love how we can completely customize InstaShow to how we want the feed to come out. Loads quickly and responsive to all devices. A++</p>
                </div>
            </div>
            <div class="review-slide">
                <div class="review-title">
                    <img src="image/1.webp" alt="">
                    <div class="review-name">
                        <h1>Taylor Jay <i class="fab fa-facebook-f"></i></h1>
                        <p>Founder & CEO</p>
                    </div>
                </div>
                <div class="review-logo">
                    <img src="image/review1.webp" alt="">
                </div>
                <div class="rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
                <div class="review-comment">
                    <p class="review-p">Love the design and customization of InstaShow ?. We have used various Instagram apps for Shopify in the past but they would always mess up our theme. I love how we can completely customize InstaShow to how we want the feed to come out. Loads quickly and responsive to all devices. A++</p>
                </div>
            </div>
        </div>
        <div class="company-icons">
            <div class="company-icons-image">
                <img src="image/icons1.png" alt="">
            </div>
            <div class="company-icons-image">
                <img src="image/icons2.png" alt="">
            </div>
            <div class="company-icons-image">
                <img src="image/icons3.png" alt="">
            </div>
            
        </div>
    </div>
</div>
<?php include('footer.php');?>  