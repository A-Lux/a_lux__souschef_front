<?php include('header.php');?> 
<div class="subscription">
<div class="container">
 <div class="modal fade subscription-modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered question-modal" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h2>Как работает подписка?</h2>
                    </div>
                    <div class="modal-body">
                    <div class="question">
                      <div class="row">
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Как активировать подписку?
                          </a>
                          <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                              При оформлении заказа просто оставьте включенной опцию «Активировать подписку».
                            </div>
                          </div>
                        </div>
                      </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                        <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-2" role="button" aria-expanded="false" aria-controls="collapseExample-2">
                            Как управлять заказами по подписке?
                          </a>
                          <div class="collapse" id="collapseExample-2">
                            <div class="card card-body">
                              В разделе «Заказы». Изменить тип и количество блюд, а также дату и время доставки вы можете до 20:00 за 4 дня до даты доставки. Здесь же вы можете пропустить одну или несколько доставок, не отменяя подписку целиком.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-3" role="button" aria-expanded="false" aria-controls="collapseExample-3">
                            Как отменить подписку?
                          </a>
                          <div class="collapse" id="collapseExample-3">
                            <div class="card card-body">
                              В разделе «Подписки». Изменить настройки подписки или полностью отменить её вы можете до 20:00 за 4 дня до даты доставки.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-4" role="button" aria-expanded="false" aria-controls="collapseExample-4">
                            Как происходит оплата заказов по подписке?
                          </a>
                          <div class="collapse" id="collapseExample-4">
                            <div class="card card-body">
                              Деньги за заказ по активной подписке будут списаны с вашей карты в 20:00 за 4 дня до даты доставки. Утром дня списания вы получите СМС-напоминание, 
                              и сможете спокойно выбрать блюда или произвести другие необходимые вам изменения, включая отмену доставки.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-5" role="button" aria-expanded="false" aria-controls="collapseExample-5">
                            Как мне добавить дополнительные блюда к заказу по подписке?
                          </a>
                          <div class="collapse" id="collapseExample-5">
                            <div class="card card-body">
                              Деньги за заказ по активной подписке будут списаны с вашей карты в 20:00 за 4 дня до даты доставки. Утром дня списания вы получите СМС-напоминание, 
                              и сможете спокойно выбрать блюда или произвести другие необходимые вам изменения, включая отмену доставки.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-6" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-6">
                          Как привязать карту?
                          </a>
                          <div class="collapse" id="collapseExample-6">
                            <div class="card card-body">
                              В личном кабинете в разделе «Банковские карты». После оплаты любого заказа картой на нашем сайте ваша 
                              карта также будет привязана к вашему профилю, повторно вводить ее не нужно. Отвязать карту можно в этом же разделе.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-7" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-7">
                          Можно ли активировать подписку и оплачивать заказы наличными?
                          </a>
                          <div class="collapse" id="collapseExample-7">
                            <div class="card card-body">
                              Нет, подписка работает только с привязкой карты к вашему профилю на нашем сайте.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-8" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-8">
                          Что делать, если мне не нравится меню следующей недели?
                          </a>
                          <div class="collapse" id="collapseExample-8">
                            <div class="card card-body ">
                              До 20:00 за 4 дня до даты доставки вы можете поменять состав подписки в разделе "Заказы". 
                              Если вам не нравится ни одно меню, вы можете отменить ближайшую доставку.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-9" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-9">
                          Что если я уезжаю в отпуск?
                          </a>
                          <div class="collapse" id="collapseExample-9">
                            <div class="card card-body">
                              До 20:00 за 4 дня до даты доставки вы можете отменить ближайшую доставку или несколько доставок. 
                              Или совсем отменить подписку.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9 col-md-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-10" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-10">
                          Я боюсь привязывать карту. Что делать?
                          </a>
                          <div class="collapse" id="collapseExample-10">
                            <div class="card card-body">
                              Не бойтесь! Мы не храним данные банковских карт и используем безопасный зашифрованный способ передачи
                               данных (SSL-протокол), позволяющий исключить возможность перехвата ваших персональных данных.
                            </div>
                          </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
</div>
</div>
<?php include('footer.php');?> 