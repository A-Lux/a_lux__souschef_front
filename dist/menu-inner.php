<?php include('header.php');?> 
<div class="menu-inner">
    <div class="container menu-inner-container">
        <div class="slider-nav">
            <div class="menu-slide">
                <h1>Меню на 27 февраля - 4 марта</h1>
                <p>Доставка этого меню доступна только с 1 по 4 марта </p>
            </div>
            <div class="menu-slide">
                <h1>Меню на 27 февраля - 4 марта</h1>
                <p>Доставка этого меню доступна только с 1 по 4 марта </p>
            </div>
        </div>
        <div class="menu-inner-tabs">
        <ul class="nav nav-pills mb-3 dishes-nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a href="" class="choose-link">Что выбрать?</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-original-tab" data-toggle="pill" href="#pills-original" role="tab" aria-controls="pills-original" aria-selected="false">Оригинальное</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="pills-family-tab" data-toggle="pill" href="#pills-family" role="tab" aria-controls="pills-family" aria-selected="true">Семейное</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-minutes-tab" data-toggle="pill" href="#pills-minutes" role="tab" aria-controls="pills-minutes" aria-selected="false">20 минут</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-balans-tab" data-toggle="pill" href="#pills-balans" role="tab" aria-controls="pills-balans" aria-selected="false">Баланс</a>
            </li>
        </ul>
        <div class="menu-tabs-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-5">
                        <div class="tabs-left">
                        <select class="dishes-quantity">
                            <option value="">3 блюда</option>
                            <option value="">4 блюда</option>
                            <option value="">5 блюд</option>
                            <option value="">6 блюд</option>
                            <option value="">7 блюд</option>
                        </select>
                        <select class="quantity-person">
                            <option value="">1 персоны</option>
                            <option value="">2 персоны</option>
                            <option value="">4 персоны</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-7">
                        <div class="tabs-right">
                            <div class="total-price">
                                <span>3 231 ₽</span>
                            </div>
                            <div class="expand-icon">
                               <span class="expand-btn"> <img src="../dist/image/expand.svg" alt=""></span>
                                <div class="expand-wrapper">
                                    <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке 
                                    не применяется.</p>
                                    <div class="priceInfo">
                                        <span>Семейное меню 3 блюда</span>
                                        <span>4 390 ₽</span>
                                    </div>
                                    <div class="scissors">
                                        <span>Ножницы</span>
                                       <div class="delete-scissors">
                                        <span>0 ₽</span>
                                        <span class="close-panel"></span>
                                       </div>
                                    </div>
                                    <div class="priceInfo-total">
                                        <span>Итого</span>
                                        <span>3 951 ₽</span>
                                    </div>
                                </div>
                            </div>
                            <div class="dish-button">
                                <button> <span>ИЗМЕНИТЬ СОСТАВ</span></button>
                            </div>
                            <div class="button-loader">
                                <a href="checkout.php">ОФОРМИТЬ ЗАКАЗ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-tabs-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-12">
                        <div class="tabs-right">
                           <div class="row">
                               <div class="col-12">
                                <div class="expand-icon">
                                    <div class="button-loader">
                                        <a href="checkout.php">ЗАКАЗАТЬ за 5 481 ₽</a>
                                    </div>
                                    <a class="btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <img src="../dist/image/expand.svg" alt="">
                                      </a>
                                </div>
                               </div>
                               <div class="col-12">
                                <div class="dish-button">
                                    <button> <span>ИЗМЕНИТЬ СОСТАВ</span></button>
                                </div>
                               </div>
                           </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12">
                        <div class="tabs-left">
                        <select class="dishes-quantity">
                            <option value="">3 блюда</option>
                            <option value="">4 блюда</option>
                            <option value="">5 блюд</option>
                            <option value="">6 блюд</option>
                            <option value="">7 блюд</option>
                        </select>
                        <select class="quantity-person">
                            <option value="">1 персоны</option>
                            <option value="">2 персоны</option>
                            <option value="">4 персоны</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                <div class="expand-wrapper">
                                    <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке 
                                    не применяется.</p>
                                    <div class="priceInfo">
                                        <span>Семейное меню 3 блюда</span>
                                        <span>4 390 ₽</span>
                                    </div>
                                    <div class="scissors">
                                        <span>Ножницы</span>
                                       <div class="delete-scissors">
                                        <span>0 ₽</span>
                                        <span class="close-panel"></span>
                                       </div>
                                    </div>
                                    <div class="priceInfo-total">
                                        <span>Итого</span>
                                        <span>3 951 ₽</span>
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
       <div class="slide-content">
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-original" role="tabpanel" aria-labelledby="pills-original-tab">
                <div class="dishes-list">
                    <div class="container container-tabs">
                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                            <div class="item-wrapper">
                              <div class="dishes-image">
                                <img src="../dist/image/d1.jpeg" alt="">
                                <div class="menu-hashtags">
                                    <span>#Понравится детям</span>
                                    <span>#Праздничный стол</span>
                                    <span>#Россия</span>
                                </div>
                            </div>
                              <div class="about-dishes">
                                  <div class="dishes-title">
                                    <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                  </div>
                                  <div class="dishes-date">
                                    <span>Готовить на <b>1-7 день</b></span>
                                    <span><img src="../dist/image/check.svg" alt=""></span>
                                </div>
                                  <div class="dishes-icon">
                                      <div class="dish-icon">
                                        <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                      </div>
                                      <div class="dish-icon">
                                        <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                      </div>
                                      <div class="dish-icon">
                                        <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                      </div>
                                  </div>
                              </div>
                            </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/d1.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/d1.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>  
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/d1.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div> 
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/d1.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>  
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/d1.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>  
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-family" role="tabpanel" aria-labelledby="pills-family-tab">
                <div class="menu-inner-for">
                 sxdc
                </div>
            </div>
            <div class="tab-pane fade" id="pills-minutes" role="tabpanel" aria-labelledby="pills-minutes-tab">
            <div class="menu-inner-for">
                   ssdxc
                </div>
            </div>
            <div class="tab-pane fade" id="pills-balans" role="tabpanel" aria-labelledby="pills-balans-tab">...</div>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-original" role="tabpanel" aria-labelledby="pills-original-tab">
                <div class="dishes-list">
                    <div class="container container-tabs">
                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                            <div class="item-wrapper">
                              <div class="dishes-image">
                                <img src="../dist/image/fam1.jpeg" alt="">
                                <div class="menu-hashtags">
                                    <span>#Понравится детям</span>
                                    <span>#Праздничный стол</span>
                                    <span>#Россия</span>
                                </div>
                            </div>
                              <div class="about-dishes">
                                  <div class="dishes-title">
                                    <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                  </div>
                                  <div class="dishes-date">
                                    <span>Готовить на <b>1-7 день</b></span>
                                    <span><img src="../dist/image/check.svg" alt=""></span>
                                </div>
                                  <div class="dishes-icon">
                                      <div class="dish-icon">
                                        <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                      </div>
                                      <div class="dish-icon">
                                        <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                      </div>
                                      <div class="dish-icon">
                                        <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                      </div>
                                  </div>
                              </div>
                            </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/fam2.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/fam3.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>  
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/fam4.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div> 
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/fam5.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>  
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-wrapper">
                                    <div class="dishes-image">
                                      <img src="../dist/image/fam1.jpeg" alt="">
                                      <div class="menu-hashtags">
                                        <span>#Понравится детям</span>
                                        <span>#Праздничный стол</span>
                                        <span>#Россия</span>
                                    </div>
                                    </div>  
                                    <div class="about-dishes">
                                        <div class="dishes-title">
                                          <h1>Чешский гуляш из говядины с макаронами ракушками</h1>
                                        </div>
                                        <div class="dishes-date">
                                            <span>Готовить на <b>1-7 день</b></span>
                                            <span><img src="../dist/image/check.svg" alt=""></span>
                                        </div>
                                        <div class="dishes-icon">
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/cook.JPG" alt="">1700 г</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/time.JPG" alt="">35 мин</p>
                                            </div>
                                            <div class="dish-icon">
                                              <p><img src="../dist/image/shef.JPG" alt="">Готовить легко</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-family" role="tabpanel" aria-labelledby="pills-family-tab">
                <div class="menu-inner-for">
                 sxdc
                </div>
            </div>
            <div class="tab-pane fade" id="pills-minutes" role="tabpanel" aria-labelledby="pills-minutes-tab">
            <div class="menu-inner-for">
                   ssdxc
                </div>
            </div>
            <div class="tab-pane fade" id="pills-balans" role="tabpanel" aria-labelledby="pills-balans-tab">...</div>
        </div>
       </div>
    </div>

<?php include('footer.php');?> 