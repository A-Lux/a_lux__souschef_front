<?php include('header.php');?> 
<div class="compain-page">
    <div class="what-choose">
        <h1>Что выбрать?</h1>
    </div>
    <div class="container">
        <div class="compain-list">
            <table>
                <thead>
                    <tr id="choose-head" class="choose-head">
                        <th >&nbsp;</th>
                        <th class="choose-title">Оригинальное</th>
                        <th class="choose-title">Семейное</th>
                        <th class="choose-title">20 минут</th>
                        <th class="choose-title">Баланс</th>
                    </tr>
                    <tr class="actions-row">
                    <th class="actions-col">&nbsp;</th> 
                    <th class="actions-col">
                        <a href="" class="choose-button">Выбрать</a>
                    </th>
                    <th class="actions-col">
                        <a href="">Выбрать</a>
                    </th>
                    <th class="actions-col">
                        <a href="">Выбрать</a>
                    </th>
                    <th class="actions-col">
                        <a href="">Выбрать</a>
                    </th>
                    </tr>
                </thead>
                <tbody>
                  <tr class="choose-border">
                      <td class="choose-value">Цена за порцию</td>
                      <td class="choose-price">от 273 Р</td>
                      <td class="choose-price">от 234 Р</td>
                      <td class="choose-price">от 263 Р</td>
                     <td class="choose-price">от 308 Р</td>
                 </tr>
                </tbody>
                <tbody>
                    <tr class="choose-border">
                    <td class="choose-value">Размер порции</td> 
                    <td class="choose-price">~ 500 г</td>
                    <td class="choose-price">~ 500 г</td>
                    <td class="choose-price">~ 500 г</td>
                    <td class="choose-price">~ 500 г</td>
                </tr>
            </tbody>
            <tbody>
                <tr class="choose-border">
                    <td class="choose-value">Время приготовления</td>
                    <td class="choose-price">25-40 минут</td>
                    <td class="choose-price">25-40 минут</td>
                    <td class="choose-price">15-20 минут</td>
                    <td class="choose-price">25-40 минут</td>
                </tr>
            </tbody>
            <tbody>
                <tr class="choose-border">
                    <td class="choose-value">Количество дней</td> 
                    <td class="choose-price">от 3 до 7</td>
                    <td class="choose-price">от 3 до 7</td>
                    <td class="choose-price">от 3 до 7</td>
                    <td class="choose-price">от 3 до 7</td>
                </tr>
            </tbody>
            <tbody
            >
                <tr class="choose-row"
                >
                    <td
                    >Ингредиенты</td> 
                    <td class="">&nbsp;</td>
                    <td class="">&nbsp;</td>
                    <td class="">&nbsp;</td>
                    <td class="">&nbsp;</td>
                </tr> 
                <tr class="choose-border">
                    <td class="choose-value">Без свинины</td> 
                    <td class="choose-price">
                        <div class="text-center change">Можно заменить</div>
                    </td>
                    <td class="choose-price">
                        <div class="text-center change">Можно заменить</div>
                    </td>
                    <td class="choose-price">
                        <div class="text-center change">Можно заменить</div>
                    </td>
                    <td class="choose-price">
                        <div class="text-center">
                            <i class="fas fa-check"></i>
                        </div>
                    </td>
                </tr>
                <tr class="choose-border">
                    <td class="choose-value">Без жирного мяса</td>
                    <td class="choose-price">
                        <div>

                        </div>
                    </td>
                    <td class="choose-price">
                        <div class="text-center">

                        </div>
                    </td>
                    <td class="choose-price">
                        <div>

                        </div>
                    </td>
                    <td class="choose-price">
                        <div class="text-center">
                            <i class="fas fa-check"></i>
                        </div>
                    </td>
                </tr>
                <tr class="choose-border">
                    <td class="choose-value" >Премиальная рыба</td>
                    <td class="choose-price">
                    <div class="text-center">
                        <i class="fas fa-check"></i>
                    </div>
                </td>
                <td class="choose-price">
                    <div>
                        
                    </div>
                </td>
                <td class="choose-price">
                    <div class="text-center">
                        <i class="fas fa-check"></i>
                    </div>
                </td>
                <td class="choose-price">
                    <div class="text-center">
                        <i class="fas fa-check"></i>
                    </div>
                </td>
            </tr>
            <tr class="choose-border">
                <td class="choose-value">Больше свежих овощей и фруктов</td>
                <td class="choose-price"></td>
            <td class="choose-price">
                <div>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
        <tr class="choose-border">
            <td class="choose-value">Разнообразные крупы</td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
        <tr class="choose-border">
            <td class="choose-value">Без сливочного масла</td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
        <tr class="choose-border">
            <td class="choose-value">Без молочной продукции</td> 
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
        <tr class="choose-border">
            <td class="choose-value">Без соли и сахара</td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
    </tbody>
    <tbody>
        <tr class="choose-row">
            <td>Рецептура</td>
            <td class="choose-price">&nbsp;</td>
            <td class="choose-price">&nbsp;</td>
            <td class="choose-price">&nbsp;</td>
            <td class="choose-price">&nbsp;</td>
        </tr> 
        <tr class="choose-border">
            <td class="choose-value">Выбор блюд</td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
        <tr class="choose-border">
            <td class="choose-value">Без интенсивной жарки</td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div>

                </div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
        <tr class="choose-border">
            <td class="choose-value">Пониженная калорийность</td>
            <td class="choose-price">
                <div></div>
            </td>
            <td class="choose-price">
                <div></div>
            </td>
            <td class="choose-price">
                <div></div>
            </td>
            <td class="choose-price">
                <div class="text-center">
                    <i class="fas fa-check"></i>
                </div>
            </td>
        </tr>
    </tbody>
                </div>
            </table>
        </div>
        <div class="compain-list-mobile">
            <h2>Выберите типы меню</h2>
            <div class="select-div">
                <select>
                    <option>Семейное</option>
                    <option>Оригинальное</option>
                    <option>20 минут</option>
                    <option>Баланс</option>
                </select>
                <select>
                    <option>20 минут</option>
                    <option>Семейное</option>
                    <option>Оригинальное</option>
                    <option>Баланс</option>
                </select>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Цены за порцию</h1>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>от 234 Р</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>от 234 Р</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Размер порции</h1>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>~ 500 г</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>~ 500 г</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Время приготовления</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>25-40 минут</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>25-40 минут</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Количество дней</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>от 3 до 7</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>от 3 о 7</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-name">
                <h2>Ингредиенты</h2>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Без свинины</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>Можно заменить</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <p>Можно заменить</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Без жирного мяса</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Премиальная рыба</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Больше свежих овощей и фруктов</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Разнообразные крупы</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Без сливочного масла</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Без молочной продукции</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Без соли и сахара</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-name">
                <h2>Рецептура</h2>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Выбор блюд</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Без интенсивной жарки</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Пониженная калорийность</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-name">
                <h2>Подходит</h2>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Детям</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Детям</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Постящимся и веганам</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>Для снижения веса и диабетикам</h1>
                 </div>
                 <div class="row">
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?> 