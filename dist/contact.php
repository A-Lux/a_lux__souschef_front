<?php include('header.php');?> 
<?php include('modal-basket.php');?> 
<?php include('whatsapp-modal.php');?> 
<div class="contact-page">
    <div class="container">
    <div class="contact-title title">
        <h1>Контакты</h1>
    </div>
    </div>
</div>
<div class="contact">
    <div class="container">
        <div class="contact-info">
            <div class="row contact-row">
                <div class="col-xl-4 col-md-4">
                   <div class="contact-link">
                       <a href="" class="contact-icons"><i class="fas fa-phone"></i></a>
                       <a href="" class="contact-item">+7 777 686 87 07</a>
                   </div> 
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="contact-link">
                        <a href="" class="contact-icons"><i class="far fa-envelope"></i></a>
                        <a href="" class="contact-item">souschef.kz@yandex.ru</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="contact-link">
                        <a href="" class="contact-icons"><i class="fas fa-map-marker-alt"></i></a>
                        <a href="" class="contact-item">г. Алматы</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="questions">
    <div class="container">
        <div class="questions-title">
            <h2>Появились вопросы? Мы с радостью ответим на них! 🙂</h2>
        </div>
        <div class="questions-image">
            <img src="image/contact-2.png" alt="">
        </div>
        <div class="questions-content">
            <div class="row">
                <div class="col-xl-6">
                   <div class="question-inner">
                       <div class="question-btn">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Сколько могут храниться продукты?
                      </button>
                      <i class="fas fa-caret-right"></i>
                    </div>
                      <div class="collapse" id="collapseExample">
                        <div class="card card-body question-body">
                           <p>Рыбу, морепродукты и мясо птицы мы рекомендуем хранить не более 2-3 дней; говядину, баранину и свинину — не более 5-6 дней. На рецепте указано количество дней с момента получения коробки, в течение которого блюдо нужно приготовить. В нашей линейке рецепты разработаны таким образом, чтобы продукты сохраняли свежесть вплоть до дня приготовления.</p> 
                        </div>
                      </div>
                   </div> 
                   <div class="question-inner">
                    <div class="question-btn">
                    <button class="btn " type="button" data-toggle="collapse" data-target="#collapseExample-2" aria-expanded="false" aria-controls="collapseExample-2">
                        Смогу ли я приготовить блюдо, если совсем не умею готовить?
                      </button>
                      <i class="fas fa-caret-right"></i>
                    </div>
                      <div class="collapse" id="collapseExample-2">
                        <div class="card card-body question-body">
                            <p>Наши ужины может приготовить любой 🙂 </p>
                            <p> Мы подготовили пошаговые фоторецепты и все продукты: Вам не нужно разделывать мясо и взвешивать ингредиенты. Просто берете пакет с ингредиентами, готовите по рецепту и удивляете близких шикарными ужинами каждый день!</p>
                        </div>
                      </div>
                   </div> 
                   <div class="question-inner">
                    <div class="question-btn">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapseExample-3" aria-expanded="false" aria-controls="collapseExample-3">
                        Почему покупать в "СуШЕФ" не дороже, чем в продуктовом магазине?
                      </button>
                      <i class="fas fa-caret-right"></i>
                    </div>
                      <div class="collapse" id="collapseExample-3">
                        <div class="card card-body question-body">
                           <p>Потому что благодаря “СуШЕФ”, Вы экономите свое ВРЕМЯ: не нужно идти в магазин, стоять в очередях и тащить покупки до дома — всё это мы сделали за Вас! Конечно, если поехать на зеленый базар или оптовку, выйдет дешевле, чем заказывать у нас или закупаться в любом гастрономе, но сколько времени и нервов Вы потратите на чтоб собрать хотя бы одно блюдо из нашего списка?</p> 
                        </div>
                      </div>
                   </div> 
                </div>
                <div class="col-xl-6">
                    <div class="question-inner">
                        <div class="question-btn">
                        <button class="btn" type="button" data-toggle="collapse" data-target="#collapseExample-4" aria-expanded="false" aria-controls="collapseExample-4">
                            Как происходит доставка?
                          </button>
                          <i class="fas fa-caret-right"></i>
                        </div>
                          <div class="collapse" id="collapseExample-4">
                            <div class="card card-body question-body">
                               <p>Доставка по Алматы — бесплатно. Коробки с Вашими ужинами доставляет наша курьерская служба — курьеры вежливы, звонят заранее и приезжают точно в срок. Мы доставляем коробки с ужинами только по воскресеньям, в любые 2-х часовые временные интервалы с 10:00 до 22:00. Выбирайте удобное для Вас время.</p> 
                            </div>
                          </div>
                       </div> 
                       <div class="question-inner">
                        <div class="question-btn">
                        <button class="btn" type="button" data-toggle="collapse" data-target="#collapseExample-5" aria-expanded="false" aria-controls="collapseExample-5">
                            Вы привезете все продукты или что-то должно быть дома?
                          </button>
                          <i class="fas fa-caret-right"></i>
                        </div>
                          <div class="collapse" id="collapseExample-5">
                            <div class="card card-body question-body">
                               <p>Мы привезем все продукты, кроме соли, перца, оливкового и подсолнечного масла — того, что наверняка всегда есть у вас дома.</p> 
                            </div>
                          </div>
                       </div> 
                       <div class="question-inner">
                        <div class="question-btn">
                        <button class="btn" type="button" data-toggle="collapse" data-target="#collapseExample-6" aria-expanded="false" aria-controls="collapseExample-6">
                            Нас в семье трое, а у вас порции только кратные двум, как быть?
                          </button>
                          <i class="fas fa-caret-right"></i>
                        </div>
                          <div class="collapse" id="collapseExample-6">
                            <div class="card card-body question-body">
                               <p>Средний вес порции в линейке «Семейное» — 500 грамм. Таким образом, в двух порциях будет целый килограмм еды! Если вы привыкли есть средними порциями, то смело берите коробку с меню на 2 порции и делите их на троих — получится по 350 грамм на человека.</p> 
                            </div>
                          </div>
                       </div> 
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?>  