<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-6 col-md-6">
            <div class="copyright">
            <p>COPYRIGHT © 2020 | МАГАЗИН РЕЦЕПТОВ</p>
            <a href="">souschef.kz@yandex.ru</a>
            <a href="">+7 777 686 87 07</a>
        </div>
            </div>
            <div class="col-xl-3 col-6">
            <div class="footer-link">
                <a href="about.php">Как это работает?</a>
                <a href="subscription.php">Как работает подписка?</a>
                <a href="menu.php">Меню</a>
                <a href="contact.php">Контакты</a>
                <a href="account.php">Аккаунт</a>
            </div>
            </div>
            <div class="col-xl-3 col-md-6">
            <h1 class="social-title">Принимаем оплату картой:</h1>
            <div class="footer-icons footer-card">
            <a href=""><img src="image/visa.png" alt=""></a>
            <a href=""><img src="image/mastercard.png" alt=""></a>
            </div>
                <h1 class="social-title">Подписывайтесь на нас:</h1>
            <div class="footer-icons">
            <a href=""><i class="fab fa-yelp"></i></a>
            <a href=""><i class="fab fa-facebook-f"></i></a>
            <a href=""><i class="fab fa-twitter"></i></a>
            <a href=""><i class="fab fa-amazon"></i></a>
            </div>
            </div>
            <div class="col-xl-2 col-12 col-md-6">
            <div class="app-links">
                <a href=""><img src="image/google-play.svg" alt=""></a>
                <a href=""><img src="image/app-store.svg" alt=""></a>
            </div>
            </div>
        </div>
        
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/hc-offcanvas-nav@3.4.1/dist/hc-offcanvas-nav.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="js/jquery.maskedinput.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="slick/slick.js"></script>
<script src="slick/slick.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>