<?php include('header.php');?> 
<?php include('modal-basket.php');?> 
<div class="account basket">
    <div class="container">
        <div class="account-inner basket-info">
            <div class="titleDiv">
                <h1>Корзина</h1>
            </div>
            <div class="basket-content">
                <img src="image/basket.JPG" alt="">
                <p>Ваша корзина пока пуста.</p>
            </div>
            <button class="button-item">Вернуться в магазин</button>
        </div>
    </div>
</div>
<?php include('footer.php');?>  