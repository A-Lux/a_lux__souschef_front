<?php include('header.php');?> 
<div class="checkout-page">
  <div class="container checkout-container">
    <div class="back-choose">
      <a href="menu-inner.php"> <span class="back-icon"></span> К выбору меню</a>
    </div>
    <div class="checkout-panel">
    <div class="row"> 
      <div class="col-xl-8">
        <div class="checkout-left">
          <h1>Адрес и время доставки</h1>
          <div class="adress-form">
            <div class="row">
              <div class="col-xl-8 col-md-7">
                <div class="adress-select">
                <select class="select-adress" name="state" id='openmodal'>
                  <option></option>
                  <option value="openmodaloption">Добавить новый адрес</option>
                </select>
                <div id="modal" class="modal fade adress-modal" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="new-adress">
                        <h4 class="modal-title">Добавление нового адреса</h4>
                        <p>Укажите точку на карте или заполните поля</p>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="map">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor" 
                        width="763" height="300" frameborder="0"></iframe>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <div class="new-adress-info">
                          <div class="row">
                            <div class="col-xl-8">
                              <div class="new-adress-input">
                                <input type="text" placeholder="Город, улица, дом">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Квартира">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Подъезд">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Этаж">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Домофон">
                              </div>
                            </div>
                            <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Имя получателя">
                              </div>
                            </div>
                            <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" placeholder="+7 (905) 724-75-05">
                              </div>
                            </div>
                            <div class="col-xl-12">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Комментарий">
                              </div>
                            </div>
                            <div class="col-xl-12">
                              <div class="add-adress">
                                <a href="">Добавить адрес</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
              <div class="col-xl-4 col-md-5">
                <div class="adress-change">
                    <a href="" type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter-2">Изменить адрес и получателя</a>
                                    <div class='modal fade adress-modal' id="exampleModalCenter-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="new-adress">
                        <h4 class="modal-title">Изменение адреса и получателя</h4>
                        <p>Укажите точку на карте или заполните поля</p>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="map">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor" 
                        width="763" height="300" frameborder="0"></iframe>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <div class="new-adress-info">
                          <div class="row">
                            <div class="col-xl-8">
                              <div class="new-adress-input">
                                <input type="text" placeholder="Город, улица, дом">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Квартира">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Подъезд">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Этаж">
                              </div>
                            </div>
                            <div class="col-xl-4">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Домофон">
                              </div>
                            </div>
                            <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Имя получателя">
                              </div>
                            </div>
                            <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" placeholder="+7 (905) 724-75-05">
                              </div>
                            </div>
                            <div class="col-xl-12">
                            <div class="new-adress-input">
                                <input type="text" placeholder="Комментарий">
                              </div>
                            </div>
                            <div class="col-xl-12">
                              <div class="add-adress">
                                <a href="">Сохранить изменения</a>
                                <div class="delete-adress">
                                    <a href="">Удалить адрес</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
              <div class="col-xl-7 col-md-7">
                <div class="delivery">
                <select class="delivery-select">
                  <option></option>
                  <option>Добавить новый адрес</option>
                </select>
                </div>
              </div>
              <div class="col-xl-5 col-md-5">
                <div class="time-delivery">
                <select class="time-select">
                  <option></option>
                  <option>Добавить новый адрес</option>
                </select>
                </div>
              </div>
            </div>
          </div>
          <h1 class="payment-title">Способ оплаты</h1>
          <div class="payment-method">
            <div class="card-left">
              <input type="text" class="payment-input" placeholder="НОМЕР КАРТЫ">
              <input type="text" class="payment-input" placeholder="ИМЯ ВЛАДЕЛЬЦА">
              <div class="payment-info">
                <div class="row">
                  <div class="col-xl-6">
                    <div class="control">
                      <input type="text" class="payment-input" placeholder="ММ/ГГ">
                    </div>
                  </div>
                  <div class="col-xl-6">
                    <div class="control-image">
                      <img src="../dist/image/pay.png" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-right">
              <div class="card-stripe"></div>
              <div class="card-info">
                <input class="payment-input" type="text" placeholder="EMAIL(ДЛЯ ОТПРАВКИ ЧЕКА)">
              <div class="card-info-bottom">
                <input class="payment-input" type="text" placeholder="CVC/CVV">
                <span class="help-icon"> ? </span>
                <div class="tooltip-card"> 
                  <p> <b>Код</b> проверки подлинности карты (или <b>код CVC*</b>) - это дополнительный код, нанесенный на 
                    вашу дебетовую или кредитную карту. Для большинства карт (Visa, MasterCard, банковские карты и т. д.) это
                    последние три цифры числа, напечатанного в поле подписи <b>на обратной стороне карты.</b></p>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="cards-icon">
            <img src="../dist/image/cards.svg" alt="">
          </div>
          <div class="payment-text">
            <p>Для проверки подлинности карты мы спишем и вернем 1 рубль. Ваша карта будет автоматически привязана к профилю, для удобства платежей. 
              Вы в любой момент можете отвязать или поменять её на другую в личном кабинете.</p>
          </div>
          <div class="info-cardowner">
            <div class="row">
              <div class="col-xl-6 col-md-6">
                <input type="text" class="payment-input" placeholder="Екатерина">
              </div>
              <div class="col-xl-6 col-md-6">
                <input type="text" class="payment-input" placeholder="+7 (905) 724-75-05">
              </div>
              <div class="col-xl-12">
                <input type="text" class="payment-input" placeholder="КОММЕНТАРИЙ К ЗАКАЗУ">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4">
      <div class="checkout-right">
      <div class="checkout-menu-list">
        <div class="row">
          <div class="col-xl-8 col-8">
            <div class="dishes-list">
              <h1>Оригинальное меню</h1>
              <span>5 блюд , 2 персоны</span>
            </div>
          </div>
          <div class="col-xl-4 col-4">
            <div class="dishes-cost">
              <span>3790 ₽</span>
            </div>
          </div>
        </div>
        <div class="text-overflow">
          <div class="read-overflow">
            <ul>
              <li>Пицца с хамоном, трюфельной пастой, сыром пармезан и рукколой с обжаренными дольками картофеля</li>
              <li>Сытный салат с курицей, авокадо и сельдереем на пшеничной лепешке с японским сметанным соусом</li>
              <li>Куриные шашлычки сате в соусе гамадари с рисовой лапшой и салатом чука</li>
              <li>Спагетти болоньезе по оригинальному рецепту с говядиной, лемонграссом и кокосовым молоком</li>
              <li>Запечённая куриная грудка в ломтиках бекона и ризотто с трюфелем, шампиньонами и сыром пармезан</li>
            </ul>
          </div>
          <div class="spoiler-icons">
          <input type="button" class="show-dishes" value="Посмотреть блюда">
          <span class="spoiler-arrow"></span>
          </div>
        </div>
        
      </div>
      <div class="checkout-menu-list scissors">
        <div class="row">
          <div class="col-xl-9 col-9">
            <div class="dishes-list">
              <h1>Ножницы</h1>
              <span>Подарок</span>
            </div>
          </div>
          <div class="col-xl-3 col-3">
            <div class="dishes-cost">
              <span>0 ₽</span>
              <span class="close-panel"></span>
            </div>
          </div>
        </div>
        
      </div>
      <div class="checkout-menu-list">
        <div class="price-inner">
          <div class="price-subscription">
            <a href="">
              <div class="price-title">
                <div class="row">
                  <div class="col-xl-9 col-9 pr-0">
                    <h1>Цена по подписке</h1>
                    <p>Активируйте подписку и получайте скидку 10 % на меню. 
                      <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                     Подробнее</a></p>
                  </div>
                  <div class="col-xl-3 col-3 p-0">
                    <span>3411 ₽</span>
                  </div>
                </div>
              </div>
               <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered question-modal" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h2>Как работает подписка</h2>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                    <div class="question">
                      <div class="row">
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Как активировать подписку?
                          </a>
                          <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                              При оформлении заказа просто оставьте включенной опцию «Активировать подписку».
                            </div>
                          </div>
                        </div>
                      </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                        <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-2" role="button" aria-expanded="false" aria-controls="collapseExample-2">
                            Как управлять заказами по подписке?
                          </a>
                          <div class="collapse" id="collapseExample-2">
                            <div class="card card-body">
                              В разделе «Заказы». Изменить тип и количество блюд, а также дату и время доставки вы можете до 20:00 за 4 дня до даты доставки. Здесь же вы можете пропустить одну или несколько доставок, не отменяя подписку целиком.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-3" role="button" aria-expanded="false" aria-controls="collapseExample-3">
                            Как отменить подписку?
                          </a>
                          <div class="collapse" id="collapseExample-3">
                            <div class="card card-body">
                              В разделе «Подписки». Изменить настройки подписки или полностью отменить её вы можете до 20:00 за 4 дня до даты доставки.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-4" role="button" aria-expanded="false" aria-controls="collapseExample-4">
                            Как происходит оплата заказов по подписке?
                          </a>
                          <div class="collapse" id="collapseExample-4">
                            <div class="card card-body">
                              Деньги за заказ по активной подписке будут списаны с вашей карты в 20:00 за 4 дня до даты доставки. Утром дня списания вы получите СМС-напоминание, 
                              и сможете спокойно выбрать блюда или произвести другие необходимые вам изменения, включая отмену доставки.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-5" role="button" aria-expanded="false" aria-controls="collapseExample-5">
                            Как мне добавить дополнительные блюда к заказу по подписке?
                          </a>
                          <div class="collapse" id="collapseExample-5">
                            <div class="card card-body">
                              Деньги за заказ по активной подписке будут списаны с вашей карты в 20:00 за 4 дня до даты доставки. Утром дня списания вы получите СМС-напоминание, 
                              и сможете спокойно выбрать блюда или произвести другие необходимые вам изменения, включая отмену доставки.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-6" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-6">
                          Как привязать карту?
                          </a>
                          <div class="collapse" id="collapseExample-6">
                            <div class="card card-body">
                              В личном кабинете в разделе «Банковские карты». После оплаты любого заказа картой на нашем сайте ваша 
                              карта также будет привязана к вашему профилю, повторно вводить ее не нужно. Отвязать карту можно в этом же разделе.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-7" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-7">
                          Можно ли активировать подписку и оплачивать заказы наличными?
                          </a>
                          <div class="collapse" id="collapseExample-7">
                            <div class="card card-body">
                              Нет, подписка работает только с привязкой карты к вашему профилю на нашем сайте.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-8" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-8">
                          Что делать, если мне не нравится меню следующей недели?
                          </a>
                          <div class="collapse" id="collapseExample-8">
                            <div class="card card-body ">
                              До 20:00 за 4 дня до даты доставки вы можете поменять состав подписки в разделе "Заказы". 
                              Если вам не нравится ни одно меню, вы можете отменить ближайшую доставку.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-9" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-9">
                          Что если я уезжаю в отпуск?
                          </a>
                          <div class="collapse" id="collapseExample-9">
                            <div class="card card-body">
                              До 20:00 за 4 дня до даты доставки вы можете отменить ближайшую доставку или несколько доставок. 
                              Или совсем отменить подписку.
                            </div>
                          </div>
                          </div>
                        </div>
                        <div class="col-xl-2">
                          <span class="question-arrow"><img src="../dist/image/collapse-arrow.svg" alt=""></span>
                        </div>
                        <div class="col-xl-9">
                          <div class="questions">
                          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-10" role="button" aria-expanded="false" 
                          aria-controls="collapseExample-10">
                          Я боюсь привязывать карту. Что делать?
                          </a>
                          <div class="collapse" id="collapseExample-10">
                            <div class="card card-body">
                              Не бойтесь! Мы не храним данные банковских карт и используем безопасный зашифрованный способ передачи
                               данных (SSL-протокол), позволяющий исключить возможность перехвата ваших персональных данных.
                            </div>
                          </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          <div class="price">
            <div class="price-title">
              <div class="row">
                <div class="col-xl-9 col-9 pr-0">
                  <h1>Цена без подписки</h1>
                </div>
                <div class="col-xl-3 col-3 p-0">
                  <span>3790 ₽</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="checkout-list-bottom">
        <div class="checkout-btn">
          <button>ОФОРМИТЬ ЗАКАЗ</button>
        </div>
        <div class="chexbox-wrapper">
          <label class="label--checkbox">
            <input type="checkbox" class="checkbox" >  
              <p> Выбирая способ оплаты, вы даете согласие ООО «Шеф Маркет» 
                на <a href="agreement.php"> обработку персональных данных </a>, а также подтверждаете, что ознакомились с <a href="agreement.php"> пользовательским соглашением</a></p>
        </label>
        </div>
      </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>


  <?php include('footer.php');?> 