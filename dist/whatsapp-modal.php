<div class="whatsapp">
<button type="button" class="btn" data-toggle="modal" data-target="#staticBackdrop">
    <i class="fab fa-whatsapp"></i>
    <span></span>
</button>
</div>

<div class="modal fade whatsapp-modal" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <div class="modal-image">
                <img src="" alt="">
            </div>
            <div class="modal-title" id="staticBackdropLabel">   
                <h1>Александр</h1>
                <p>Помогу с выбором блюд и по другим вопросам :)</p>
            </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="message">
             <h1>Александр</h1>
             <p class="wpp-text">Приветствую! ?</p>
             <p >Чем могу вам помочь?</p>
             <span id="time-sending"></span>
         </div>
        </div>
        <div class="modal-footer">
          <a href=""><i class="fab fa-whatsapp"></i> Начать чат</a>
        </div>
      </div>
    </div>
  </div>