<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>Сушеф</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/hc-offcanvas-nav@3.4.1/dist/hc-offcanvas-nav.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="font/css/all.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="header">
        <div class="header-descktop">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                   <div class="header-left">
                   <div class="logo">
                        <img src="image/logo.png" alt="">
                    </div>
                    <div class="tagline">
                        <h1>Готовить - одно удовольствие</h1>
                    </div>
                   </div>
                </div>
                <div class="col-xl-8">
                    <div class="header-links">
                        <a class='link-active' href="index.php">Главная</a>
                        <a href="about.php">Как это работает?</a>
                        <a href="subscription.php">Как работает подписка?</a>
                        <a href="menu.php">Меню</a>
                        <a href="contact.php">Контакты</a>
                        <a href="account.php">Аккаунт</a>
                        <li class="nav-item">
                            <span class="basket-count">0</span>
                            <a class="nav-link icons"  href="#">0  <span>₸</span></a>
                            <a href=""><i class="fa fa-shopping-basket" aria-hidden="true"></i></a>
                         </li>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-logo">
        <div class="mobile-logo-content">
        <img src="image/logo.png" alt="">
        <div class="tagline">
            <h1>Готовить - одно удовольствие</h1>
        </div>
        </div>
    </div>
    <div class="basket-mobile">
    <span class="basket-count">0</span>
    <a href=""><i class="fa fa-shopping-basket" aria-hidden="true"></i></a>
    </div>
        <a class="mobile-menu-toggle js-toggle-menu hamburger-menu" href="#">
  </a>
            <nav class="mobile-nav-wrap" role="navigation">
      <ul class="mobile-header-nav">
      <li><a href="#">ГЛАВНАЯ</a></li>
      <li><a href="about.php">КАК ЭТО РАБОТАЕТ?</a></li>
      <li><a href="subscription.php">КАК РАБОТАЕТ ПОДПИСКА?</a></li>
      <li><a href="menu.php">МЕНЮ</a></li>
      <li><a href="contact.php">КОНТАКТЫ</a></li>
      <li class="account-mob">
          <a href="#" class="ast-menu-toggle">АККАУНТ
          <button class="ast-menu-btn"></button>
          </a>
      <ul class="sub-menu">
	<li id="menu-item-1">
        <a href=""><i class="fas fa-chevron-left"></i>Мой аккаунт</a>
    </li>
	<li id="menu-item-2">
        <a href=""><i class="fas fa-chevron-left"></i>Корзина</a>
    </li>
</ul>
    </li>
    </ul>
  </nav>
    </div>
    

