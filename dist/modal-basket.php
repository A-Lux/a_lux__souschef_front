<div id="slide-panel">
  <div class="slide-inner">
    <a href="#" class="btn " id="opener"> <img src="image/modal-basket.JPG" alt=""></a>
    <span class="basket-count">0</span>
    <div class="trigger">
        <div class="modal-basket">
            <div class="modal-title">
                <h1>Ваша корзина</h1>
            </div>
            <div class="modal-basket-content">
                <p>Ваша корзина пуста.</p>
            </div>
            <div class="modal-basket-footer">
                <h2>Предварительный итог: <span>0  ₸</span></h2>
                <p>Чтобы узнать стоимость доставки, перейдите к оформлению заказа.</p>
                <a href="">Продолжить покупки</a>
            </div>
        </div>
    </div>

  </div>
</div>